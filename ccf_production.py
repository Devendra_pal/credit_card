import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pylab import rcParams
import warnings
import mlflow
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
warnings.filterwarnings('ignore')
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
# from sklearn.metrics import accuracy_score
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix
import mlflow.pyfunc
# import dvc.api

mlflow.set_tracking_uri("http://65.0.194.230:5000")

model_name = 'CCF_RF1'
model_version_uri = "models:/{model_name}/production".format(model_name=model_name)
loaded_model = mlflow.pyfunc.load_model(model_version_uri)

# path2 = 'Data//next_month.csv'
# repo2 = 'C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection' 
# version2 = 'v2'

# data_url2 = dvc.api.get_url(
#     path=path2,
#     repo= repo2,
#     rev= version2  
# )
# path = 'Data//credit_card.csv'
# repo = 'C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection' 
# version = 'v1'

# data_url = dvc.api.get_url(
#     path=path,
#     repo= repo,
#     rev= version  
# )
def calculate_psi(expected, actual, buckettype='bins', buckets=10, axis=0):
    def psi(expected_array, actual_array, buckets):
        def scale_range (input, min, max):
            input += -(np.min(input))
            input /= np.max(input) / (max - min)
            input += min
            return input
        breakpoints = np.arange(0, buckets + 1) / (buckets) * 100
        if buckettype == 'bins':
            breakpoints = scale_range(breakpoints, np.min(expected_array), np.max(expected_array))
        elif buckettype == 'quantiles':
            breakpoints = np.stack([np.percentile(expected_array, b) for b in breakpoints])
        expected_percents = np.histogram(expected_array, breakpoints)[0] / len(expected_array)
        actual_percents = np.histogram(actual_array, breakpoints)[0] / len(actual_array)
        def sub_psi(e_perc, a_perc):
            if a_perc == 0:
                a_perc = 0.0001
            if e_perc == 0:
                e_perc = 0.0001
            value = (e_perc - a_perc) * np.log(e_perc / a_perc)
            return(value)
        psi_value = np.sum(sub_psi(expected_percents[i], actual_percents[i]) for i in range(0, len(expected_percents)))

        return(psi_value)

    if len(expected.shape) == 1:
        psi_values = np.empty(len(expected.shape))
    else:
        psi_values = np.empty(expected.shape[axis])

    for i in range(0, len(psi_values)):
        if len(psi_values) == 1:
            psi_values = psi(expected, actual, buckets)
        elif axis == 0:
            psi_values[i] = psi(expected[:,i], actual[:,i], buckets)
        elif axis == 1:
            psi_values[i] = psi(expected[i,:], actual[i,:], buckets)

    return(psi_values)
mlflow.set_experiment('Credit_card-fraud')

if __name__ == "__main__":
    data=pd.read_csv('C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection//Data//credit_card.csv')
    data2 = pd.read_csv('C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection//Data//next_month.csv')
    # data2=pd.read_csv(data_url2, sep=",")
    # data = pd.read_csv(data_url,sep=",")
    raw,col = data2.shape
    X=data2.drop(['Class'],axis=1)
    sc = RobustScaler()
    training_set_scale_cm = sc.fit_transform(X)
    y=data2['Class']
    # X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.30,random_state=123)

    client = mlflow.tracking.MlflowClient(tracking_uri = 'http://65.0.194.230:5000')
    mlflow.set_tracking_uri('http://65.0.194.230:5000')
    mlflow.set_experiment('Credit_card-fraud')
    with mlflow.start_run(run_name='prod') as mlops_run:
        # rfc=RandomForestClassifier()
        prediction=loaded_model.predict(training_set_scale_cm)
        accuracy = accuracy_score(y,prediction)
        precision = precision_score(y, prediction)
        recall = recall_score(y, prediction)
        f1_score= (2*precision*recall)/(precision+recall)
        print('accuracy=', accuracy)
        print('f1_score=',f1_score)
        print('precision=',precision)
        print('recall=',recall)
        mlflow.log_param('Model_Name', 'Random_forest')
        mlflow.log_param('Data_version','v1')
        mlflow.log_param('input_row',raw)
        mlflow.log_param('input_cols', col)
        mlflow.log_metric("accuracy",accuracy)
        mlflow.log_metric("f1_score",f1_score)
        mlflow.log_metric("precision",precision)
        mlflow.log_metric("recall",recall)
        with open("metrics.txt", "w") as outfile:
            outfile.write("accuracy: " + str(accuracy) + "\n")
            outfile.write("precision: " + str(precision) + "\n")
            outfile.write("recall: " + str(recall) + "\n")
            outfile.write("f1_score: " + str(f1_score) + "\n")
        top_feature_list = ['V3', 'V4',  'V11', 'V12',  'V14', 'V15', 'V16', 'V17', 'V18',
        'V25', 'V26']
        psi_list = []
        for feature in top_feature_list:
                # Assuming you have a validation and training set
                psi_t = calculate_psi(data2[feature], data[feature])
                psi_list.append(psi_t)      
                print(feature,psi_t)
                mlflow.log_param(feature,psi_t)
                if psi_t>0.05:
                    print("Need to retrain the model")
                    with open("PSI.txt", "w+") as outfile:
                        outfile.write("feature: " + str(psi_t) + "\n")
                    # mlflow.log_artifact('ccf_v1.pkl')
                    


    