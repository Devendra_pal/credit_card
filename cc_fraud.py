import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pylab import rcParams
import warnings
import mlflow
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
warnings.filterwarnings('ignore')
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from urllib.parse import urlparse
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix
import pickle
import dvc.api

path = 'Data//credit_card.csv'
repo = 'https://gitlab.com/Devendra_pal/credit_card.git' 
# version = 'cc1'

data_url = dvc.api.get_url(
    path=path,
    repo= repo,
    # rev= version  
)
mlflow.set_experiment('Credit_card-fraud')

# def eval_metrics(actual, pred):
#     rmse = np.sqrt(mean_squared_error(actual,pred))
#     mae= mean_absolute_error(actual,pred)
#     r2 = r2_score(actual,pred)
#     return rmse, mae, r2

if __name__ == "__main__":

    
    # data=pd.read_csv('C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection//Data//credit_card.csv')
    data=pd.read_csv(data_url, sep=",")
    raw,col = data.shape
    fraud_cases=len(data[data['Class']==1])
    non_fraud_cases=len(data[data['Class']==0])
    fraud=data[data['Class']==1]
    genuine=data[data['Class']==0]

    X=data.drop(['Class'],axis=1)
    y=data['Class']
    X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.30,random_state=123)
    sc = RobustScaler()
    training_set_scaled = sc.fit_transform(X_train)
    test_set_scaled = sc.fit_transform(X_test)
    client = mlflow.tracking.MlflowClient(tracking_uri = 'http://65.0.194.230:5000')
    mlflow.set_experiment('Credit_card-fraud')
    mlflow.set_tracking_uri('http://65.0.194.230:5000')
    mlflow.set_experiment('Credit_card-fraud')
    with mlflow.start_run(run_name='test1') as mlops_run:
        rfc=RandomForestClassifier()
        model=rfc.fit(training_set_scaled,y_train)
        prediction=model.predict(test_set_scaled)
        accuracy = accuracy_score(y_test,prediction)
        precision = precision_score(y_test, prediction)
        recall = recall_score(y_test, prediction)
        f1_score= (2*precision*recall)/(precision+recall)
        with open("metrics.txt", "w") as outfile:
            outfile.write("accuracy: " + str(accuracy) + "\n")
            outfile.write("precision: " + str(precision) + "\n")
            outfile.write("recall: " + str(recall) + "\n")
            outfile.write("f1_score: " + str(f1_score) + "\n")

        print('accuracy=', accuracy)
        print('f1_score=',f1_score)
        print('precision=',precision)
        print('recall=',recall)
        # filename = 'ccf_v1.pkl'
        # pickle.dump(model, open(filename, 'wb'))
        mlflow.log_param('Model_Name', 'Random_forest')
        mlflow.log_param('Data_version','v1')
        mlflow.log_param('input_row',raw)
        mlflow.log_param('input_cols', col)
        mlflow.log_metric("accuracy",accuracy)
        mlflow.log_metric("f1_score",f1_score)
        mlflow.log_metric("precision",precision)
        mlflow.log_metric("recall",recall)
        # mlflow.log_artifact('ccf_v1.pkl')
        tracking_url_type_store = urlparse(mlflow.get_artifact_uri()).scheme
        print(tracking_url_type_store)
        if tracking_url_type_store != 'file':
            mlflow.sklearn.log_model(model ,'model', registered_model_name = 'CCF_RF1')
        else:
            pass


